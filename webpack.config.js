/**
 * Created by Muhammet Ali on 2.04.2017.
 */
var path = require('path');

module.exports = {
    entry: "./src/index.js",
    output: {
        path: __dirname,
        filename: "bundle.js"
    },
    module: {
        loaders: [
            { test: /\.css$/, loader: "style-loader!css-loader" },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                use: [
                    'url-loader?limit=10000',
                    'img-loader'
                ]
            }
        ]
    },
    resolve: {
        // alias: {
        //     amcharts: path.resolve('./src/amcharts.js'),
        //     amcharts3: path.resolve('./node_modules/amcharts/dist/amcharts')
        // }
    }
};

