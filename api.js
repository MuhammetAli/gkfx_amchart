/**
 * Created by Muhammet Ali on 2.04.2017.
 */
var express = require('express');
var app = express();
var port = process.env.PORT || 8000;
var request = require('request');
var util = require('util');
var each = require('foreach');

// Access Control Permission
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    next();
});


//request option
var reqOptions = {
    uri: 'http://www.barchartmarketdata.com/data-samples/getHistoryUSFuturesEOD.json',
    method: 'GET',
    json: true
};

var makePrety = function (rawData) {
    var output = [];
    each(rawData, function (value, key, array) {

        output.push({
            "symbol": value.symbol,
            "date": value.tradingDay,
            "open": value.open.toString(),
            "high": value.high.toString(),
            "low": value.low.toString(),
            "close": value.close.toString()
        });

    });
    return output;
};


app.get('/pro-candle', function (req, res) {

    request(reqOptions, function(error, response, body){
        if(error) console.log(error);
        else res.json(makePrety(body.results));
    });

});


app.listen(port);