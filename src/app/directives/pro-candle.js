/**
 * Created by Muhammet Ali on 2.04.2017.
 */
var apiRefPath = 'pro-candle';

var option = {
    "type": "serial",
    "path": "node_modules/amcharts/dist/amcharts/",
    "theme": "light",
    "titles": [{
        "text": "ESZ5",
        "size": 18
    }],
    "dataDateFormat":"YYYY-MM-DD",
    "valueAxes": [ {
        "position": "left"
    } ],
    "graphs": [ {
        "id": "g1",
        "proCandlesticks": true,
        "balloonText": "Symbol:<b>[[symbol]]</b><br>Open:<b>[[open]]</b><br>Low:<b>[[low]]</b><br>High:<b>[[high]]</b><br>Close:<b>[[close]]</b><br>",
        "closeField": "close",
        "fillColors": "#7f8da9",
        "highField": "high",
        "lineColor": "#7f8da9",
        "lineAlpha": 1,
        "lowField": "low",
        "fillAlphas": 0.9,
        "negativeFillColors": "#db4c3c",
        "negativeLineColor": "#db4c3c",
        "openField": "open",
        "title": "symbol",
        "type": "candlestick",
        "valueField": "close"
    } ],
    "chartScrollbar": {
        "graph": "g1",
        "graphType": "line",
        "scrollbarHeight": 30
    },
    "chartCursor": {
        "valueLineEnabled": true,
        "valueLineBalloonEnabled": true
    },
    "categoryField": "date",
    "categoryAxis": {
        "parseDates": true
    },
    "dataProvider": [],
    "export": {
        "enabled": true,
        "position": "bottom-right"
    }
};


export default function (chartService) {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            "chartId": "@id",
            "addClass": "@addClass"
        },
        link: function (scope) {
            chartService.getData(scope.chartId, option, apiRefPath);
        },
        template: '<div id="{{ chartId }}" class="{{ addClass }}"></div>'
    }

};