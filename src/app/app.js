/**
 * Created by Muhammet Ali on 2.04.2017.
 */
// angular lib
import angular from 'angular';

import chartService from './services/chart-service';
import proCandleChart from './directives/pro-candle';

// angular app
export default angular.module('app', [])
    .service('chartService', chartService)
    .directive('proCandle', proCandleChart);
