/**
 * Created by Muhammet Ali on 2.04.2017.
 */

export default function ($http) {

    return {
        "getData" : function (chartId, option, apiRefPath) {

            $http.get('http://127.0.0.1:8000/' + apiRefPath)
                .then(function successCallback(response) {
                    option.dataProvider = response.data;
                    var chart = AmCharts.makeChart(chartId, option);

                }, function errorCallback(response) {

                    console.log("chart-service error:", response);

                });
        }
    };

};